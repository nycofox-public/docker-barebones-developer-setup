FROM nginx:stable-alpine

ADD ./nginx/nginx.conf /etc/nginx/nginx.conf
ADD ./nginx/default.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /var/www/html

RUN addgroup -g 1000 devuser && adduser -G devuser -g devuser -s /bin/sh -D devuser

RUN chown devuser:devuser /var/www/html