FROM php:7.4-fpm-alpine

ADD ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN addgroup -g 1000 devuser && adduser -G devuser -g devuser -s /bin/sh -D devuser

RUN mkdir -p /var/www/html

RUN chown devuser:devuser /var/www/html

WORKDIR /var/www/html

RUN docker-php-ext-install pdo pdo_mysql