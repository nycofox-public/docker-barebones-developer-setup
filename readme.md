## To start the containers
    docker-compose up -d --build

### Web server
Requires the folder `src/public` to exist.

### To run composer
    docker-compose run --rm composer XXXX
    
### To run npm install
    docker-compose run --rm npm install